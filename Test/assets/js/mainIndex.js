function initComponent() {}

function leerCheckbox() {
  var checkbox = $("input:checkbox[name=dolor_Cabeza]:checked").val();
  alert(checkbox);
}

function calcularRiesgo() {
  var dateNow = new Date(Date.now() - 5 * 60 * 60 * 1000)
    .toISOString()
    .split("-");
  var date = dateNow[0].slice(0, 2) + dateNow[1] + dateNow[2].slice(0, 2);

  var nombre = $("#firstname").val();
  var apellido = $("#lastname").val();
  var codigo = $("#codigo").val();
  var email = $("#email").val();

  var fiebre = $("input:checkbox[name=fiebre]:checked").val();
  if (fiebre == undefined) {
    fiebre = 0;
  }
  var tos = $("input:checkbox[name=tos]:checked").val();
  if (tos == undefined) {
    tos = 0;
  }
  var fatiga = $("input:checkbox[name=fatiga]:checked").val();
  if (fatiga == undefined) {
    fatiga = 0;
  }
  var dolor_Garganta = $("input:checkbox[name=dolor_Garganta]:checked").val();
  if (dolor_Garganta == undefined) {
    dolor_Garganta = 0;
  }
  var congestion = $("input:checkbox[name=congestion]:checked").val();
  if (congestion == undefined) {
    congestion = 0;
  }
  var escalofrios = $("input:checkbox[name=escalofrios]:checked").val();
  if (escalofrios == undefined) {
    escalofrios = 0;
  }
  var dolor_Cabeza = $("input:checkbox[name=dolor_Cabeza]:checked").val();
  if (dolor_Cabeza == undefined) {
    dolor_Cabeza = 0;
  }
  var diarrea_Vomito = $("input:checkbox[name=diarrea_Vomito]:checked").val();
  if (diarrea_Vomito == undefined) {
    diarrea_Vomito = 0;
  }
  var perdida_Sentido = $("input:checkbox[name=perdida_Sentido]:checked").val();
  if (perdida_Sentido == undefined) {
    perdida_Sentido = 0;
  }
  var dolor_Muscular = $("input:checkbox[name=dolor_Muscular]:checked").val();
  if (dolor_Muscular == undefined) {
    dolor_Muscular = 0;
  }
  var dificultad_Respirar = $(
    "input:checkbox[name=dificultad_Respirar]:checked"
  ).val();
  if (dificultad_Respirar == undefined) {
    dificultad_Respirar = 0;
  }
  var ningun_Sintoma = $("input:checkbox[name=ningun_Sintoma]:checked").val();
  if (ningun_Sintoma == undefined) {
    ningun_Sintoma = 0;
  }

  var contacto_Contagiado = $(
    "input:checkbox[name=contacto_Contagiado]:checked"
  ).val();
  if (contacto_Contagiado == undefined) {
    contacto_Contagiado = 0;
  }
  var viaje_Internacional = $(
    "input:checkbox[name=viaje_Internacional]:checked"
  ).val();
  if (viaje_Internacional == undefined) {
    viaje_Internacional = 0;
  }
  var viaje_Nacional = $("input:checkbox[name=viaje_Nacional]:checked").val();
  if (viaje_Nacional == undefined) {
    viaje_Nacional = 0;
  }
  var trabajador_Salud = $(
    "input:checkbox[name=trabajador_Salud]:checked"
  ).val();
  if (trabajador_Salud == undefined) {
    trabajador_Salud = 0;
  }
  var contacto_Salud = $("input:checkbox[name=contacto_Salud]:checked").val();
  if (contacto_Salud == undefined) {
    contacto_Salud = 0;
  }
  var ningun_Contacto = $("input:checkbox[name=ningun_Contacto]:checked").val();
  if (ningun_Contacto == undefined) {
    ningun_Contacto = 0;
  }

  var medio_Transporte = document.getElementById("medio_Transporte").value;

  var temperatura_Registrada = document.getElementById("temperatura_Registrada")
    .value;

  var valor_Riesgo =
    parseFloat(fiebre) +
    parseFloat(tos) +
    parseFloat(fatiga) +
    parseFloat(dolor_Garganta) +
    parseFloat(congestion) +
    parseFloat(escalofrios) +
    parseFloat(dolor_Cabeza) +
    parseFloat(diarrea_Vomito) +
    parseFloat(perdida_Sentido) +
    parseFloat(dolor_Muscular) +
    parseFloat(dificultad_Respirar) +
    parseFloat(ningun_Sintoma) +
    parseFloat(contacto_Contagiado) +
    parseFloat(viaje_Internacional) +
    parseFloat(viaje_Nacional) +
    parseFloat(trabajador_Salud) +
    parseFloat(contacto_Salud) +
    parseFloat(ningun_Contacto) +
    parseFloat(medio_Transporte) +
    parseFloat(temperatura_Registrada);

  var riesgo;

  if (valor_Riesgo >= 1) {
    riesgo = "Riesgo_Alto";
    document.getElementById("qrcode").innerHTML = "";
    $("#nivel_Riesgo").text("Riesgo Alto")
    document.getElementById("nivel_Riesgo").style.color = "#C41611";
    $("#texto_Riesgo").text("Evita los lugares públicos. Quedate en casa, cuídate y cuida a tu familia")
    $("#qrcode").append('<IMG style="width:50%" SRC="https://png2.cleanpng.com/sh/583721825b7ef8003b6de5fc35c3ae3f/L0KzQYq3VsMxN6VvfpH0aYP2gLBuTgNucZ1qkZ92YYPuPbb0jBpqNZV0hdNybj24dIKCUcI3a2I8TNc5MT60RYW3Ucc2PmI6TqMEMkS1QIW3WcU2NqFzf3==/kisspng-smiley-mask-emoji-domain-5d19126c174e01.1540175615619242040955.png">')


  } else if (valor_Riesgo >= 0.5 && valor_Riesgo < 1) {
    riesgo = "Riesgo_Medio";
    $("#nivel_Riesgo").text("Riesgo Medio")
    document.getElementById("nivel_Riesgo").style.color = "#E96600";
    $("#texto_Riesgo").text("La mejor cura es la prevención protegete y proteje a los demás, recuerda seguir todas las medidas de precaución")
    document.getElementById("qrcode").innerHTML = "";
    new QRCode(
      document.getElementById("qrcode"),
      "http://127.0.0.1:5500/Test/ingresoUsuarios.html?nombre=" +
        nombre.replace(" ", "_") +
        "_" +
        apellido.replace(" ", "_") +
        "&riesgo=" +
        riesgo +
        "&codigo=" +
        codigo +
        "&fecha=" +
        date
    );
    var qrCodeBaseUri = "https://api.qrserver.com/v1/create-qr-code/?",
      params = {
        data:
          "http://127.0.0.1:5500/Test/ingresoUsuarios.html?nombre=" +
          nombre.replace(" ", "_") +
          "_" +
          apellido.replace(" ", "_") +
          "&riesgo=" +
          riesgo +
          "&codigo=" +
          codigo +
          "&fecha=" +
          date,
        size: "200x200",
        margin: 0,
        // more configuration parameters ...
        download: 1,
      };

    window.location.href = qrCodeBaseUri + $.param(params);
  } else {
    riesgo = "Riesgo_Bajo";
    $("#nivel_Riesgo").text("Riesgo Bajo")
    document.getElementById("nivel_Riesgo").style.color = "#37bd03";
    $("#texto_Riesgo").text("Continua con las buenas practicas para estar protegido")
    document.getElementById("qrcode").innerHTML = "";
    new QRCode(
      document.getElementById("qrcode"),
      "http://127.0.0.1:5500/Test/ingresoUsuarios.html?nombre=" +
        nombre.replace(" ", "_") +
        "_" +
        apellido.replace(" ", "_") +
        "&riesgo=" +
        riesgo +
        "&codigo=" +
        codigo +
        "&fecha=" +
        date
    );
    var qrCodeBaseUri = "https://api.qrserver.com/v1/create-qr-code/?",
      params = {
        data:
          "http://127.0.0.1:5500/Test/ingresoUsuarios.html?nombre=" +
          nombre.replace(" ", "_") +
          "_" +
          apellido.replace(" ", "_") +
          "&riesgo=" +
          riesgo +
          "&codigo=" +
          codigo +
          "&fecha=" +
          date,
        size: "200x200",
        margin: 0,
        // more configuration parameters ...
        download: 1,
      };

    window.location.href = qrCodeBaseUri + $.param(params);
  }
}
