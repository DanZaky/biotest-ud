function initComponents() {
    var nombre = getParameterByName('nombre').replace("_", " ");
    nombre = nombre.replace("_", " ");
    var riesgo = getParameterByName('riesgo').replace("_", " ");
    var codigo = getParameterByName('codigo').replace("_", " ");
    var fecha = getParameterByName('fecha').replace("_", " ");
    var dateNow = new Date(Date.now() - 5 * 60 * 60 * 1000).toISOString().split("-");
    var date = dateNow[0].slice(0, 2) + dateNow[1] + dateNow[2].slice(0, 2) 


    if (riesgo == "Riesgo Bajo") {
        document.getElementById("backCard").style.backgroundColor = "#37bd03";

    } else {
        document.getElementById("backCard").style.backgroundColor = "#d6d30c";

    }

    if(fecha != date){
        document.getElementById("backCard").style.backgroundColor = "#d60c0c";
        $("#nivelRiesgo").text("La fecha no corresponde con el día de realización")
        $("#codigoUsuario").text(codigo)


    }else{
        $("#nombreUsuario").text(nombre)
        $("#nivelRiesgo").text(riesgo)
        // $("#nombreUsuario").val(key)
        $("#codigoUsuario").text(codigo)
    }

    
}

/**
 * @param String name
 * @return String
 */
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}